#!/bin/bash
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install docker.io git -y
sudo usermod -aG docker ubuntu
sudo wget https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
sudo rpm -ivh amazon-ssm-agent.rpm
sudo systemctl enable amazon-ssm-agent